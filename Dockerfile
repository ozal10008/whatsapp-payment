FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
WORKDIR /app
ENTRYPOINT ["java"]
CMD ["-jar", "/app/whatsapp-payment-ms-1.0.d12f5f5.jar"]
