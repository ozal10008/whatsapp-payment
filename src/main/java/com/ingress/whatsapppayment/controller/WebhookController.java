package com.ingress.whatsapppayment.controller;

import com.ingress.whatsapppayment.dto.WebhookDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/webhook")
public class WebhookController {
    @PostMapping // http://localhost:8080/api/webhook
    public ResponseEntity<WebhookDTO> print(@RequestBody WebhookDTO requestBody) {
        System.out.println("###### Webhook #####" + requestBody.toString());
        return new ResponseEntity<>(requestBody, HttpStatus.OK);
    }

}
