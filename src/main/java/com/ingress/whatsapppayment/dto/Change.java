package com.ingress.whatsapppayment.dto;
public class Change {
    private Value value;
    private String field;

    public Change() {
    }

    public Change(Value value, String field) {
        this.value = value;
        this.field = field;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "Change{" +
                "value=" + value +
                ", field='" + field + '\'' +
                '}';
    }
}
