package com.ingress.whatsapppayment.dto;

import java.util.List;

public class Entry {
    private String id;
   private List<Change> changes;

    public Entry() {
    }

    public Entry(String id, List<Change> changes) {
        this.id = id;
        this.changes = changes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Change> getChanges() {
        return changes;
    }

    public void setChanges(List<Change> changes) {
        this.changes = changes;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "id='" + id + '\'' +
                ", changes=" + changes +
                '}';
    }
}

