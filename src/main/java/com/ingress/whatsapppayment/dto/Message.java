package com.ingress.whatsapppayment.dto;

public class Message {
    private Context context;
    private String from;
    private String id;
    private String timestamp;
    private String type;
    private Button button;

    public Message() {
    }

    public Message(Context context, String from, String id, String timestamp, String type, Button button) {
        this.context = context;
        this.from = from;
        this.id = id;
        this.timestamp = timestamp;
        this.type = type;
        this.button = button;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    @Override
    public String toString() {
        return "Message{" +
                "context=" + context +
                ", from='" + from + '\'' +
                ", id='" + id + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", type='" + type + '\'' +
                ", button=" + button +
                '}';
    }
}
