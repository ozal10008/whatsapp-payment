package com.ingress.whatsapppayment.dto;

import java.util.List;

public class WebhookDTO {
    private List<Entry> entry;
    private String object;

    public WebhookDTO() {
    }

    public WebhookDTO(List<Entry> entry, String object) {
        this.entry = entry;
        this.object = object;
    }

    public List<Entry> getEntry() {
        return entry;
    }

    public void setEntry(List<Entry> entry) {
        this.entry = entry;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "WebhookDTO{" +
                "entry=" + entry +
                ", object='" + object + '\'' +
                '}';
    }
}
